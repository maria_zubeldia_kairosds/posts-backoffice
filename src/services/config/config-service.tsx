import { Config } from "./config";

let baseUrl: string = "";

export class ConfigService {

    async load() {
        //console.log("estoy en fetch");
        const response = await fetch('/posts-backoffice/config/config.json');
        const jsonResponse: Config = await response.json();
        baseUrl = jsonResponse.baseUrl;
        console.log("baseUrl config", baseUrl);

        return baseUrl;
    }

    get config() {
        return baseUrl;
    }
}

const serviceInstanceSingleton = new ConfigService();
Object.freeze(serviceInstanceSingleton);
export default serviceInstanceSingleton;