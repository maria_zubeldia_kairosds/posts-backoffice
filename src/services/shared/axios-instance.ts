import axios from "axios";

export function generateInterceptor() {
  //console.log("declarando axios instance");

  axios.interceptors.request.use((config) => {
    //console.log("creando interceptor request axios");

    const token = localStorage.getItem("react-token");
    const headers = { Authorization: `Bearer ${token}` };
    config.headers = headers;

    return config;
  });
}
