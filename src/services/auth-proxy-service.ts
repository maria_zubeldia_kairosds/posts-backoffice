import axios from "axios";
import { Token, UserDto, UserSignUpRequest } from "../types";
import ConfigService from "./config/config-service";

const config = {
  headers: {
    "Content-Type": "application/json",
  },
};


export async function login(credentials: UserDto): Promise<Token | any> {
  //const BASE_URL_ENVIRONMENT = ConfigService.config;
  //console.log("url environment",BASE_URL_ENVIRONMENT);
  //const LOGIN_END = "api/login";
  
  //const LOGIN_URL = `${BASE_URL_ENVIRONMENT}/${LOGIN_END}`;
  //console.log("full login url",LOGIN_URL);

  const response = await axios.post(`${ConfigService.config}/api/login`, credentials, config);
  const token = response.data.token;
  localStorage.setItem("react-token", token);

  return token;
}

export async function getRole(): Promise<string> {
  //const BASE_URL_ENVIRONMENT = ConfigService.config;

  const AUTH_URL = `${ConfigService.config}/api/auth/role/me`;

  const role = await axios.get(AUTH_URL);
  return role.data.role;
}

export async function signUp(
  credentials: UserSignUpRequest
): Promise<string | any> {
  //const BASE_URL_ENVIRONMENT = ConfigService.config;

  //const SIGNUP_URL = `${BASE_URL_ENVIRONMENT}/api/sign-up`;

  const response = await axios.post(`${ConfigService.config}/api/sign-up`, credentials, config);
  return response.data;
}
