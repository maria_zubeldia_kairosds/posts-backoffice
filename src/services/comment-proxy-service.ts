import axios from "axios";
import { CommentDto } from "../types";
import ConfigService from "./config/config-service";

//console.log(COMMENT_URL);


export async function getCommentByIdRepository(commentId: string) {
  //const COMMENT_URL = ConfigService.config;

  const response = await axios.get(
    `${ConfigService.config}/api/posts/comments/${commentId}`
  );
  return response.data;
}

export async function addCommentRepository(comment: CommentDto, idPost: string | undefined): Promise<CommentDto> {
  //const COMMENT_URL = ConfigService.config;

  const response = await axios.post(
    `${ConfigService.config}/api/posts/${idPost}/comments`, comment
  );

  return response.data;
}

export async function deleteCommentRepository(commentId: string) {
  //const COMMENT_URL = ConfigService.config;

  const response = await axios.delete(
    `${ConfigService.config}/api/posts/comments/${commentId}`
  );
  return response.data;
}


/*
const mapFromDtoToModelComment = (
  apiCommentResponse: CommentDto[]
): CommentModel[] | any => {
  return apiCommentResponse.map((commentFromApi) => {
    const {
      id,
      email: emailAuthor,
      content: content,
      timestamp: timestamp,
    } = commentFromApi;

    return {
      id,
      emailAuthor,
      content,
      timestamp,
    };
  });
};
*/