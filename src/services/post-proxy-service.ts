import axios from "axios";
import { PostDto, PostModel } from "../types";
import ConfigService from "./config/config-service";

//const configUrl = ConfigService.config;
//console.log("configUrl posts proxy", configUrl);

export async function getAllPostsRepository(): Promise<PostDto[]> {
  //const configUrl = ConfigService.config;
  const response = await axios.get(`${ConfigService.config}/api/posts`);
  return response.data;
}

export async function addPostRepository(
  post: PostModel
): Promise<PostDto | undefined> {
  //const configUrl = ConfigService.config;
  const response = await axios.post(`${ConfigService.config}/api/posts`, post);
  return response.data;
}

export async function deletePostRepository(postId: string): Promise<Object> {
  //const configUrl = ConfigService.config;
  const response = await axios.delete(`${ConfigService.config}/api/posts/${postId}`);
  return response.data;
}

export async function updatePostRepository(post: PostModel): Promise<PostDto> {
  //const configUrl = ConfigService.config;
  const response = await axios.put(`${ConfigService.config}/api/posts`, post);
  return response.data;
}

export async function getPostByIdRepository(
  postId: string | undefined
): Promise<PostModel> {
  //const configUrl = ConfigService.config;
  const response = await axios.get(`${ConfigService.config}/api/posts/${postId}`);

  return response.data;
}

/*
export const mapFromDtoToModel = (
  apiResponse: any
): PostModel[] | any => {
  return apiResponse.map((postFromApi: PostDto) => {
    const {
      id,
      title: title,
      content: content,
      comments: comments,
    } = postFromApi;

    return {
      id,
      title,
      content,
      comments,
    };
  });
};*/
