import FormComment from "../../components/FormComment";
import { Header } from "../../components/Header";
import useComments from "../../hooks/useComments";
import usePosts from "../../hooks/usePosts";
import { ArticleContainer } from "./PostDetailStyles";


const PostDetail = () => {

    const {
        clickedPostData,
        comments,
        isFormActive,
        inputValues,
        handleOpenForm,
        handleAddComment,
        handleClear,
        handleChange,
        handleDelete
    } = useComments();

    const { isAllowed } = usePosts();


    return (
        <>
            <Header />

            <main>
                <h2>PostDetail Component</h2>
                <h3 id={clickedPostData?.id}>Post title: {clickedPostData?.title}</h3>
                <p>Content: {clickedPostData?.content}</p>
                <h4>Comentarios: </h4>

                {comments.map(comment => {
                    return (
                        <ArticleContainer key={comment.id} id={comment.id}>

                            <p>{comment.content}</p>
                            <p>{comment.email}</p>
                            <p>{comment.timestamp}</p>
                            {
                                (isAllowed == true)
                                    ? <button id={comment.id} onClick={handleDelete}>delete</button>
                                    : null
                            }
                        </ArticleContainer>)
                }
                )}

                {isFormActive
                    ?
                    <>
                        <FormComment
                            handleChange={handleChange}
                            inputValues={inputValues}
                            handleAddComment={handleAddComment}
                            handleClear={handleClear}
                            handleOpenForm={handleOpenForm}
                        />
                    </>

                    : <><button onClick={handleOpenForm}>Add comment</button></>
                }

            </main>

        </>

    )
}

export default PostDetail

/*
                    <form className="comment_form" onSubmit={e => e.preventDefault()}>
                        <h4>Add comment: </h4>

                        <input
                            onChange={handleChange}
                            type="text"
                            name="content"
                            placeholder="your comment"
                            value={inputValues.content}

                        />
                        <input type="email"
                            name="email"
                            placeholder="your email"
                            value={inputValues.email}
                            onChange={handleChange}
                        />
                        <button onClick={handleAddComment}>Send</button>
                        <button onClick={handleClear}>Clear</button>
                        <button onClick={handleOpenForm}>Minimizar</button>
                    </form>

*/