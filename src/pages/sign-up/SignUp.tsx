import { useState } from 'react';
import { Header } from '../../components/Header';
import { signUp } from '../../services/auth-proxy-service';
import { FormSignUpContainer, InputTextSignUp, MainContainer, SelectForm, SignButton } from "./SignUpStyles";

const SignUp = () => {

    const [user, setUser] = useState({
        email: "",
        password: "",
        role: "",
    });

    const callSignUp = (user: any) => {

        try {
            const useCase = signUp(user)
        } catch (err) {
            console.log(err);
        }

    }

    const handleSignUpSubmit = async (ev: any) => {
        ev.preventDefault();

        callSignUp(user)
    }

    const handleChange = (ev: any) => {

        setUser({
            ...user,
            [ev.target.name]: ev.target.value,
        })
    }

    return (
        <>
            <Header />

            <MainContainer>
                <h3>Sign Up</h3>

                <FormSignUpContainer onSubmit={handleSignUpSubmit}>
                    <InputTextSignUp
                        required
                        type="email"
                        name="email"
                        placeholder="type your email"
                        onChange={handleChange}
                    >
                    </InputTextSignUp>
                    <InputTextSignUp
                        required
                        type="password"
                        name="password"
                        placeholder="type your password"
                        onChange={handleChange}
                    >
                    </InputTextSignUp>
                    <SelectForm required name="role" onChange={handleChange}>
                        <option value="">Select a role</option>
                        <option value="USER">User</option>
                        <option value="AUTHOR">Author</option>

                    </SelectForm>

                    <SignButton>Sign Up</SignButton>

                </FormSignUpContainer>

            </MainContainer>

        </>

    )
}

export default SignUp