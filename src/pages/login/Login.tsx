import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Header } from "../../components/Header";
import { RoleContext } from "../../contexts/token.context";
import { getRole, login } from "../../services/auth-proxy-service";
import { FormLoginContainer, InputTextLogin, LoginButton, MainContainer, SmallContainer, SmallContainerLink } from "./LoginStyles";

const Login = () => {

    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const ctxRole = useContext(RoleContext)

    const navigate = useNavigate()

    const handleLoginSubmit = async (ev: any) => {
        ev.preventDefault();

        try {
            const newUser = await login({
                email: userName,
                password: password
            })

            await setNewRole();
            setUserName("");
            setPassword("");
            navigate("/posts")

        } catch (err) {
            console.log(err);
        }
    }
    const setNewRole = async () => {
        const newRole = await getRole()
        console.log("newrole", newRole);

        ctxRole?.setRole(newRole)
    }

    return (
        <>
            <Header />

            <MainContainer>
                <h3>Login</h3>

                <FormLoginContainer onSubmit={handleLoginSubmit}>
                    <InputTextLogin type="email"
                        value={userName}
                        name="username"
                        placeholder="username"
                        onChange={(ev) => setUserName(ev.target.value)}
                    >
                    </InputTextLogin>
                    <InputTextLogin
                        type="password"
                        value={password}
                        name="password"
                        placeholder="password"
                        onChange={(ev) => setPassword(ev.target.value)}
                    >
                    </InputTextLogin>

                    <LoginButton>Login</LoginButton>

                </FormLoginContainer>
                <SmallContainer>
                    <span>You don't have an account?</span>
                    <SmallContainerLink to="/signup">Sign up now</SmallContainerLink>
                </SmallContainer>

            </MainContainer>
        </>

    )
}

export default Login