import { useNavigate } from "react-router-dom";
import { PostModel } from "../../types";

interface Props {
    isAllowed: boolean,
    postsInfo: Array<PostModel>,
    handleDelete: (ev: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    handleUpdate: (ev: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const PostsList = ({ isAllowed, postsInfo, handleUpdate, handleDelete }: Props) => {

    const navigate = useNavigate();
    console.log(isAllowed, "postlist");


    return (
        <>
            <h3>Listado de posts</h3>
            {
                (isAllowed == true)
                    ? postsInfo.map(post => {
                        return (
                            <li key={post.id} id={post.id} data-id={post.id}>
                                <article onClick={() => navigate(`/posts/${post.id}`)}  >
                                    <h4>Titulo: {post.title}</h4>
                                    <p>Contenido: {post.content}</p>
                                </article>
                                <button id={post.id} onClick={(ev) => handleUpdate(ev)}>update</button>
                                <button id={post.id} onClick={(ev) => handleDelete(ev)}>delete</button>

                            </li>
                        )
                    }
                    )
                    : postsInfo.map(post => {
                        return (
                            <li key={post.id} id={post.id} data-id={post.id}>
                                <article onClick={() => navigate(`/posts/${post.id}`)}  >
                                    <h4>Titulo: {post.title}</h4>
                                    <p>Contenido: {post.content}</p>
                                </article>

                            </li>
                        )
                    }
                    )

            }
        </>
    )
}

export default PostsList;

/*



*/