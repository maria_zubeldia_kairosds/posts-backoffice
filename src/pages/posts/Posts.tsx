import FormPost from "../../components/FormPost";
import { Header } from "../../components/Header";
import LogOut from "../../components/LogOut";
import usePosts from "../../hooks/usePosts";
import PostsList from "./PostsList";

export const Posts = () => {

  //hook llamadas api y control form
  const {
    postData,
    inputValues,
    isEditing,
    isAllowed,
    handleSend,
    handleClear,
    handleChange,
    handleDelete,
    handleUpdate,
  } = usePosts();

  return (
    <>
      <Header />

      <LogOut />
      <h2>Componente posts</h2>

      <section>
        {
          (isAllowed == true)
            ?
            <FormPost
              handleChange={handleChange}
              inputValues={inputValues}
              handleSend={handleSend}
              isEditing={isEditing}
              handleClear={handleClear}
            />

            : null
        }

      </section>

      <section>
        <PostsList isAllowed={isAllowed} postsInfo={postData} handleUpdate={handleUpdate} handleDelete={handleDelete} />
      </section>
    </>

  )
}