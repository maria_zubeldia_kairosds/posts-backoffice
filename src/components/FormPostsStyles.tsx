import styled from "styled-components"

export const FormPostContainer = styled.form`
	padding-left: 15%;
	padding-right: 15%;

    display: flex;
    flex-direction: column;
    padding-bottom: 0.5rem;
`

export const InputTextPost = styled.input`
    padding-bottom: 0.4rem;
    margin-bottom: 0.5rem;
    border-radius:6px;
	border:1px solid #dcdcdc;
`

export const TextareaPost = styled.textarea`
	padding-bottom: 0.4rem;
    margin-bottom: 0.5rem;
    border-radius:6px;
	border:1px solid #dcdcdc;
`

export const PostButton = styled.button`
    box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:linear-gradient(to bottom, var(--subtle-color) 5%, var(--subtle-color) 100%);
	background-color: var(--subtle-color);
	border-radius:6px;
	border:1px solid #dcdcdc;
	display:inline-block;
	cursor:pointer;
	color:#666666;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;
	margin-right: 0.5rem;

    &:hover {
        background:linear-gradient(to bottom, #f6f6f6 5%, #ffffff 100%);
	    background-color:#f6f6f6;
    }
    &:active {
        position:relative;
	    top:1px;
    }

`

export const ClearButton = styled.button`
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:linear-gradient(to bottom, var(--main-font-color) 5%, var(--main-font-color) 100%);
	background-color: var(--main-font-color);
	border-radius:6px;
	border:1px solid #dcdcdc;
	display:inline-block;
	cursor:pointer;
	color: var(--main-bg-color);
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;

    &:hover {
        background: var(--highlight-color);
    }
    &:active {
        position:relative;
	    top:1px;
    }
`