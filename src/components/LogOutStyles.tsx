import styled from "styled-components"


export const ButtonLogout = styled.button`
    margin-top: 1rem;
    float: right;
    
    box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:linear-gradient(to bottom, var(--subtle-color) 5%, var(--subtle-color) 100%);
	background-color: var(--subtle-color);
	border-radius:6px;
	border:1px solid #dcdcdc;
	display:block;
	cursor:pointer;
	color:#666666;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;
	//margin-right: 0.5rem;

    &:hover {
        background:linear-gradient(to bottom, #f6f6f6 5%, #ffffff 100%);
	    background-color:#f6f6f6;
    }
    &:active {
        position:relative;
	    top:1px;
    }
`