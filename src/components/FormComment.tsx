import { ClearCommentButton, CommentSendButton, FormCommentContainer, HideButton, InputTextComment } from "./FormCommentStyles"

const FormComment = ({ handleChange, inputValues, handleAddComment, handleClear, handleOpenForm }: any) => {
    return (

        <FormCommentContainer className="comment_form" onSubmit={e => e.preventDefault()}>
            <h4>Add comment: </h4>

            <InputTextComment
                onChange={handleChange}
                type="text"
                name="content"
                placeholder="your comment"
                value={inputValues.content}
            />
            <InputTextComment type="email"
                name="email"
                placeholder="your email"
                value={inputValues.email}
                onChange={handleChange}
            />
            <div>
                <CommentSendButton onClick={handleAddComment}>Send</CommentSendButton>
                <ClearCommentButton onClick={handleClear}>Clear</ClearCommentButton>
                <HideButton onClick={handleOpenForm}>Minimizar</HideButton>
            </div>


        </FormCommentContainer>

    )
}

export default FormComment