import { ClearButton, FormPostContainer, InputTextPost, PostButton, TextareaPost } from "./FormPostsStyles"


const FormPost = ({ handleChange, inputValues, handleSend, isEditing, handleClear }: any) => {
    return (
        <>
            <h3>Form componente</h3>

            <FormPostContainer onSubmit={e => e.preventDefault()}>
                <InputTextPost
                    onChange={handleChange}
                    name="title"
                    value={inputValues.title}
                    type="text"
                    placeholder="title"
                    minLength={5}
                    maxLength={30}></InputTextPost>

                <TextareaPost onChange={handleChange} name="content" value={inputValues.content} placeholder="content" minLength={50} maxLength={300}></TextareaPost>
                <InputTextPost onChange={handleChange} name="id" value={inputValues.id} type="text" placeholder={inputValues.id} disabled></InputTextPost>

                <div>
                    <PostButton onClick={handleSend}>{isEditing == true ? "Update" : "Save"}</PostButton>
                    <ClearButton onClick={handleClear}>Clear</ClearButton>
                </div>

            </FormPostContainer>
        </>

    )
}

export default FormPost