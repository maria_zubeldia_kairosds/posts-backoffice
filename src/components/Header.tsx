import { Link } from 'react-router-dom';
import styled from 'styled-components';

const HeaderStyled = styled.header`
display: flex;
justify-content: space-between;
align-items: center;
background-color: var(--highlight-color);

`

const NavStyled = styled.nav`
  display: flex;
  padding: 1rem;
  justify-content: space-around;
`

const Title = styled.h1`
  padding-left: 1rem;
`

const NavLink = styled(Link)`
  padding-right: 0.5rem;
  color: var(--subtle-color);

  &:hover,
  &:focus{
    color: var(--main-font-color);
  }
  &:active{
    color: red;
  }
`

export function Header(): JSX.Element {
  return (
    <HeaderStyled>

      <Title>
        Posts Backoffice
      </Title>

      <NavStyled>
        <NavLink to="/">Login</NavLink>
        <NavLink to="/posts">Posts</NavLink>

      </NavStyled>


    </HeaderStyled>
  )
}
