import { useNavigate } from "react-router-dom";
import { ButtonLogout } from "./LogOutStyles";

import usePosts from "../hooks/usePosts";

const LogOut = () => {

    const { ctxRole } = usePosts()
    const navigate = useNavigate()

    const handleLogout = () => {
        localStorage.clear();
        ctxRole?.setRole("")
        navigate("/")
    }
    return (
        <>
            <ButtonLogout onClick={handleLogout}>Logout</ButtonLogout>
        </>
    )
}

export default LogOut