import { Route, Routes } from 'react-router-dom';
import { RoleProvider } from '../contexts/token.context';
import Login from '../pages/login/Login';
import PostDetail from '../pages/post-detail/PostDetail';
import { Posts } from '../pages/posts/Posts';
import SignUp from '../pages/sign-up/SignUp';


export function Router(): JSX.Element {

    return (
        <>                
        <RoleProvider>

            <Routes>

                <Route path="/" element={<Login />} />
                <Route path="/signup" element={<SignUp />} />
                <Route path="/posts" element={<Posts />} />
                <Route path="/posts/:postDetail" element={<PostDetail />} />
                <Route path="*" element={<div>Not found</div>} />

            </Routes>
        </RoleProvider>

        </>
    )
}
