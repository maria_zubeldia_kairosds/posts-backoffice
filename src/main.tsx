import ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import App from './App'
import './index.css'
import './resets.css'
import ConfigService from './services/config/config-service'
import { generateInterceptor } from './services/shared/axios-instance'


ConfigService.load()
  .then(() => {

    //ConfigService.config;
    //console.log("main tsx config", ConfigService.config);

    generateInterceptor();

    //console.log("init config");

    ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
      <BrowserRouter basename='posts-backoffice'>
        <App />
      </BrowserRouter>);
  });
  
/*
const renderAll = () => {

}
renderAll();
*/
