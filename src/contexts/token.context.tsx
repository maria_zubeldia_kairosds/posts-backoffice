import { createContext, SetStateAction, useState } from "react";

interface role {
    role: string;
    setRole: React.Dispatch<SetStateAction<string>>;
}

export const RoleContext = createContext<role | null>(null);
RoleContext.displayName = "role";


export const RoleProvider = ({ children }: any) => {
    const [role, setRole] = useState("");

    return (
        <RoleContext.Provider value={{ role, setRole }}>
            {children}
        </RoleContext.Provider>
    );
};

