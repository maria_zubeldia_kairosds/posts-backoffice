export interface PostModel {
  key?: string;
  id?: string;
  title: string;
  content: string;
  comments?: CommentDto[];
}

export interface PostDto {
  id?: string;
  title: string;
  content: string;
  comments?: CommentDto[];
}

export interface CommentDto {
  id?: string,
  email: string,
  content: string,
  timestamp?: string
}

export interface CommentModel {
  id?: string,
  emailAuthor: string,
  content: string,
  timestamp?: string
}

export interface idPost {
  idPost: string;
}

export interface idComment {
  idComment: string
}

export interface Token {
  token: string;
}

export interface UserDto {
  email: string;
  password: string;
  //role: string;
}

export interface UserSignUpRequest {
  email: string;
  password: string;
  role: string
}
