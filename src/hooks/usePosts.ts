import { useContext, useEffect, useState } from "react";
import { RoleContext } from "../contexts/token.context";
import {
  addPostRepository,
  deletePostRepository,
  getAllPostsRepository,
  updatePostRepository
} from "../services/post-proxy-service";
import { PostModel } from "../types";

const usePosts = () => {
  const [postData, setPostData] = useState<PostModel[]>([]);

  //guarda si usuario es admin o author para renderizar botones y forms
  const [isAllowed, setIsAllowed] = useState(true);

  const ctxRole = useContext(RoleContext);
  const getPosts = async () => {
    const response = await getAllPostsRepository();

    setPostData(response);
  };

  useEffect(() => {
    getPosts();

    checkIfAllowed();
  }, []);

  interface FormState {
    inputValues: PostModel;
  }

  interface FormProps {
    handleNewSub: (newSub: PostModel) => void;
  }

  const INITIAL_STATE = {
    id: "",
    title: "",
    content: "",
  };

  const [inputValues, setInputValues] =
    useState<FormState["inputValues"]>(INITIAL_STATE);

  const [isEditing, setIsEditing] = useState(false);

  const handleSend = () => {
    //controla si se hace llamada a addPost o updatePostRepository
    if (isEditing) {
      updatePostRepository(inputValues);
      handleNewSub(inputValues);
    } else {
      //if !inputValues => no enviar info
      //else => llamar repos
      addPostRepository(inputValues);
      handleNewSub(inputValues);
    }
  };

  const handleClear = () => {
    setInputValues(INITIAL_STATE);
    setIsEditing(false);
  };

  const handleChange = (
    ev: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setInputValues({
      ...inputValues,
      [ev.target.name]: ev.target.value,
    });
  };

  const handleNewSub = (newPost: PostModel): void => {
    setPostData((postsData) => [...postsData, newPost]);
  };

  const filteredById = (postId: string) => {
    //comprobar que el id está en el array de postData
    //sacar el post que tiene el mismo id

    setPostData(postData.filter((element) => element.id !== postId));
  };

  const handleDelete = (
    ev: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    const postId = ev.currentTarget.id;
    filteredById(postId);
    deletePostRepository(postId);
  };

  const handleUpdate = (
    ev: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ): void => {
    const postIdToUpdate = ev.currentTarget.id;
    setIsEditing(true);

    //find postById
    const findId: PostModel | undefined = postData.find(
      (element: PostModel) => element.id === postIdToUpdate
    );

    if (!findId) {
      console.log("id no existe");
    }
    //convierte el findIdPost a modelPost
    const postToUpdate: PostModel = {
      id: findId!.id,
      title: findId!.title,
      content: findId!.content,
    };

    //pinta valores del postToUpdate en el formulario
    setInputValues({
      id: postToUpdate.id,
      title: postToUpdate.title,
      content: postToUpdate.content,
    });
    filteredById(postIdToUpdate);
  };

  const checkIfAllowed = () => {
    if (ctxRole?.role == "USER") {
      setIsAllowed(false);
    }
  };

  return {
    postData,
    setPostData,
    inputValues,
    setInputValues,
    isEditing,
    setIsEditing,
    handleSend,
    handleClear,
    handleChange,
    handleDelete,
    handleUpdate,
    ctxRole,
    isAllowed,
    setIsAllowed,
  };
};

export default usePosts;
