import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {
  addCommentRepository,
  deleteCommentRepository
} from "../services/comment-proxy-service";
import { getPostByIdRepository } from "../services/post-proxy-service";
import { CommentDto, PostModel } from "../types";

interface FormState {
  inputValues: CommentDto;
}

const INITIAL_STATE = {
  content: "",
  email: "",
  id: "",
  timestamp: "",
};

const useComments = () => {
  const { postDetail } = useParams<{ postDetail: string | undefined }>();

  const [clickedPostData, setClickedPostData] = useState<PostModel>();
  const [comments, setComments] = useState<CommentDto[]>([]);
  const [isFormActive, setIsFormActive] = useState(false);
  const [inputValues, setInputValues] =
    useState<FormState["inputValues"]>(INITIAL_STATE);

  const getPostInfo = async (postId: string | undefined) => {
    if (postId !== undefined) {
      const detailPost = await getPostByIdRepository(postDetail);
      setClickedPostData(detailPost);
      detailPost.comments && setComments(detailPost.comments);
    }
    return;
  };

  useEffect(() => {
    getPostInfo(postDetail);
  }, []);

  const handleOpenForm = () => {
    console.log("estoy en open form");
    if (!isFormActive) {
      setIsFormActive(true);
    } else {
      setIsFormActive(false);
    }
  };

  const handleAddComment = () => {
    const idPost = postDetail;
    handleNewSub(inputValues);
    addCommentRepository(inputValues, idPost);
    setInputValues(INITIAL_STATE);
  };

  const handleClear = () => {
    setInputValues(INITIAL_STATE);
  };

  const handleChange = (
    ev: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setInputValues({
      ...inputValues,
      [ev.target.name]: ev.target.value,
    });
  };

  const handleNewSub = (newComment: CommentDto): void => {
    setComments((commentData) => [...commentData, newComment]);
  };

  const handleDelete = (ev: any) => {
    const id = ev.target.id;
    deleteCommentRepository(id);
    setComments(comments.filter((element) => element.id !== id));
  };

  return {
    postDetail,
    clickedPostData,
    setClickedPostData,
    comments,
    setComments,
    isFormActive,
    setIsFormActive,
    inputValues,
    setInputValues,
    getPostInfo,
    handleOpenForm,
    handleAddComment,
    handleClear,
    handleChange,
    handleDelete,
  };
};

export default useComments;
